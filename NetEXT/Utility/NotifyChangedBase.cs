﻿using System;

namespace NetEXT.Utility
{
    /// <summary>Abstract class that provides an uniform way to raise an event that something changed.</summary>
    [Serializable]
    public abstract class NotifyChangedBase
    {
        /// <summary>This event is raised when the derived object has changed in some way.</summary>
        [field: NonSerialized]
        public event Action NotifyChanged;
        /// <summary>Called by derived classes to raise the notify changed event.</summary>
        protected void OnNotifyChanged()
        {
            if (NotifyChanged != null) NotifyChanged();
        }
    }
}
