﻿using System;
using System.Collections.Generic;

namespace NetEXT.Utility
{
    /// <summary>Class that can contain and invoke multiple callbacks that have different signatures.</summary>
    /// <remarks>This is useful when you want to selectivly invoke A(int); or B(string); depending on what arguments were passed.</remarks>
    public class DynamicDelegate
    {
        #region Variables
        private List<Delegate> _delegates = new List<Delegate>();
        #endregion

        #region Properties
        /// <summary>Gets an array of all currently registered delegates.</summary>
        public Delegate[] Delegates
        {
            get
            {
                return _delegates.ToArray();
            }
        }
        #endregion

        #region Functions
        /// <summary>Registers a callback to be invoked.</summary>
        /// <param name="Delegate">Callback that will be registered</param>
        public void RegisterDelegate(Delegate Delegate)
        {
            _delegates.Add(Delegate);
        }
        /// <summary>Removes a callback.</summary>
        /// <param name="Delegate">Callback to be removed</param>
        public void UnregisterDelegate(Delegate Delegate)
        {
            _delegates.Remove(Delegate);
        }
        /// <summary>Removes all currently registered delegates.</summary>
        public void ClearDelegates()
        {
            _delegates.Clear();
        }
        /// <summary>Invokes the first registered empty delegate.</summary>
        /// <returns>Result of the callback.</returns>
        public object InvokeDelegate() { return InvokeDelegate(new object[0]); }
        /// <summary>Invokes the first registered delegate with a matching single parameter.</summary>
        /// <param name="Parameter">Parameter that is passed to the callback</param>
        /// <returns>Result of the callback.</returns>
        public object InvokeDelegate(object Parameter) { return InvokeDelegate(new object[] { Parameter }); }
        /// <summary>Invokes the first registered delegate that matches the parameter list.</summary>
        /// <param name="Parameters">Parameter array that is passed to the callback</param>
        /// <returns>Result of the callback.</returns>
        public object InvokeDelegate(object[] Parameters)
        {
            foreach (var del in _delegates)
            {
                var args = del.Method.GetParameters();
                if (args.Length == Parameters.Length)
                {
                    bool matches = true;
                    for (int i = 0; i < args.Length; i++)
                    {
                        if (args[i].ParameterType != Parameters[i].GetType())
                        {
                            matches = false;
                            break;
                        }
                    }
                    if (matches) return del.DynamicInvoke(Parameters);
                }
            }
            throw new Exception("No matching delegate found for the provided Parameter(s)");
        }
        /// <summary>Invokes the first registered empty delegate with the matching return type.</summary>
        /// <typeparam name="T">Return type of the callback</typeparam>
        /// <returns>Result of the callback.</returns>
        public T InvokeDelegate<T>() { return InvokeDelegate<T>(new object[0]); }
        /// <summary>Invokes the first registered delegate with a matching single parameter and return type.</summary>
        /// <typeparam name="T">Return type of the callback</typeparam>
        /// <param name="Parameter">Parameter that is passed to the callback</param>
        /// <returns>Result of the callback.</returns>
        public T InvokeDelegate<T>(object Parameter) { return InvokeDelegate<T>(new object[] { Parameter }); }
        /// <summary>Invokes the first registered delegate that matches the parameter list and return type.</summary>
        /// <typeparam name="T">Return type of the callback</typeparam>
        /// <param name="Parameters">Parameter array that is passed to the callback</param>
        /// <returns>Result of the callback.</returns>
        public T InvokeDelegate<T>(object[] Parameters)
        {
            foreach (var del in _delegates)
            {
                var args = del.Method.GetParameters();
                if (args.Length == Parameters.Length && typeof(T) == del.Method.ReturnType)
                {
                    bool matches = true;
                    for (int i = 0; i < args.Length; i++)
                    {
                        if (args[i].ParameterType != Parameters[i].GetType())
                        {
                            matches = false;
                            break;
                        }
                    }
                    if (matches) return (T)del.DynamicInvoke(Parameters);
                }
            }
            throw new Exception("No matching delegate found for the provided Parameter(s) and/or Return Type");
        }
        #endregion

        #region Operators
        public static DynamicDelegate operator +(DynamicDelegate LHS, DynamicDelegate RHS)
        {
            DynamicDelegate combined = new DynamicDelegate();
            foreach (var del in LHS.Delegates) { combined.RegisterDelegate(del); }
            foreach (var del in RHS.Delegates) { combined.RegisterDelegate(del); }
            return combined;
        }
        #endregion
    }
}
