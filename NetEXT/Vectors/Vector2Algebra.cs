﻿using System;
using System.Diagnostics;
using NetEXT.MathFunctions;
using SFML.Window;
using SFML.Graphics;
using SFML.System;

namespace NetEXT.Vectors
{
    /// <summary>Algebraic operations on two-dimensional vectors (dot product, vector length and angles, ...)</summary>
    public static class Vector2Algebra
    {
        #region Functions
        /// <summary>Returns the length of the 2D vector.</summary>
        public static float Length(Vector2f Vector)
        {
            return (float)Trigonometry.SqrRoot(SquaredLength(Vector));
        }
        /// <summary>Adapts a vector so that its length matches the new length.</summary>
        public static Vector2f Length(Vector2f Vector, float VectorLength)
        {
            Debug.Assert(Vector != new Vector2f());
            return Vector * Length(Vector);
        }
        /// <summary>Returns the square of the vector's length.</summary>
        public static float SquaredLength(Vector2f Vector)
        {
            return DotProduct(Vector, Vector);
        }
        /// <summary>Returns a vector with same direction as the argument, but with length 1.</summary>
        public static Vector2f UnitVector(Vector2f Vector)
        {
            Debug.Assert(Vector != new Vector2f());
            return Vector / Length(Vector);
        }
        /// <summary>Returns the polar angle.</summary>
        public static float PolarAngle(Vector2f Vector)
        {
            Debug.Assert(Vector != new Vector2f());
            return (float)Trigonometry.ArcTan2(Vector.X, Vector.Y);
        }
        /// <summary>Sets the polar angle of the specified vector.</summary>
        public static Vector2f PolarAngle(Vector2f Vector, float Angle)
        {
            float length = Length(Vector);
            return new Vector2f(length * (float)Trigonometry.Cos(Angle), length * (float)Trigonometry.Sin(Angle));
        }
        /// <summary>Rotates the vector by the given angle (in degrees).</summary>
        public static Vector2f Rotate(Vector2f Vector, float Angle)
        {
            float cos = (float)Trigonometry.Cos(Angle);
            float sin = (float)Trigonometry.Sin(Angle);
            return new Vector2f((cos * Vector.X) - (sin * Vector.Y), (sin * Vector.X) + (cos * Vector.Y));
        }
        /// <summary>Returns a perpendicular vector.</summary>
        public static Vector2f PerpendicularVector(Vector2f Vector)
        {
            return new Vector2f(-Vector.Y, Vector.X);
        }
        /// <summary>Computes the signed angle from LHS to RHS.</summary>
        public static float SignedAngle(Vector2f LHS, Vector2f RHS)
        {
            Debug.Assert(LHS != new Vector2f() && RHS != new Vector2f());
            return (float)Trigonometry.ArcTan2(CrossProduct(LHS, RHS), DotProduct(LHS, RHS));
        }
        /// <summary>Computes the dot product of two 2D vectors.</summary>
        public static float DotProduct(Vector2f LHS, Vector2f RHS)
        {
            return (LHS.X * RHS.X) + (LHS.Y * RHS.Y);
        }
        /// <summary>Computes the cross product of two 2D vectors.</summary>
        public static float CrossProduct(Vector2f LHS, Vector2f RHS)
        {
            return (LHS.X * RHS.Y) - (LHS.Y * RHS.X);
        }
        /// <summary>Returns the component-wise product of LHS and RHS.</summary>
        public static Vector2f CWiseProduct(Vector2f LHS, Vector2f RHS)
        {
            return new Vector2f(LHS.X * RHS.X, LHS.Y * RHS.Y);
        }
        /// <summary>Returns the component-wise quotient of LHS and RHS.</summary>
        public static Vector2f CWiseQuotient(Vector2f LHS, Vector2f RHS)
        {
            Debug.Assert(RHS.X != 0 && RHS.Y != 0);
            return new Vector2f(LHS.X / RHS.X, LHS.Y / RHS.Y);
        }
        /// <summary>Returns the projection of a vector onto an axis.</summary>
        public static Vector2f ProjectedVector(Vector2f Vector, Vector2f Axis)
        {
            Debug.Assert(Axis != new Vector2f());
            return DotProduct(Vector, Axis) / SquaredLength(Axis) * Axis;
        }
        #endregion
    }
}
