﻿using System;
using NetEXT.MathFunctions;
using SFML.Window;
using SFML.Graphics;
using SFML.System;

namespace NetEXT.Vectors
{
    /// <summary>Vector in polar coordinate system.</summary>
    /// <remarks>2D vector which stores its components in polar instead of cartesian coordinates.</remarks>
    public struct PolarVector
    {
        #region Variables
        public float R;
        public float A;
        #endregion

        #region Constructors/Destructors
        /// <summary>Constructs a polar vector with specified radius and angle.</summary>
        /// <param name="RValue">Radial component (length of the vector)</param>
        /// <param name="AValue">Angular component in degrees.</param>
        public PolarVector(float RValue, float AValue)
        {
            R = RValue;
            A = AValue;
        }
        /// <summary>Constructs a polar vector from a cartesian SFML vector.</summary>
        /// <param name="Vector">Cartesian (x, y) vector being converted to polar coordinates</param>
        public PolarVector(Vector2i Vector) : this(new Vector2f(Vector.X, Vector.Y)) { }
        /// <summary>Constructs a polar vector from a cartesian SFML vector.</summary>
        /// <param name="Vector">Cartesian (x, y) vector being converted to polar coordinates</param>
        public PolarVector(Vector2f Vector)
        {
            R = Vector2Algebra.Length(Vector);
            if (Vector.X == 0 && Vector.Y == 0) A = 0;
            else A = Vector2Algebra.PolarAngle(Vector);
        }
        #endregion

        #region Operators
        public static implicit operator Vector2f(PolarVector Vector)
        {
            return new Vector2f(Vector.R * (float)Trigonometry.Cos(Vector.A), Vector.R * (float)Trigonometry.Sin(Vector.A));
        }
        public static implicit operator Vector2i(PolarVector Vector)
        {
            return new Vector2i((int)Math.Round(Vector.R * (float)Trigonometry.Cos(Vector.A), 0), (int)Math.Round(Vector.R * (float)Trigonometry.Sin(Vector.A), 0));
        }
        #endregion
    }
}
