﻿using System;
using SFML.System;

namespace NetEXT.Particles
{
    /// <summary>Base class on which custom particle emitters can be built.</summary>
    public abstract class EmitterBase
    {
        /// <summary>Called so the emitter can emit particles.</summary>
        /// <param name="ParticleSystem">Particle system that will recieve the new particles</param>
        /// <param name="DeltaTime">Elapsed time since last emit call</param>
        public abstract void EmitParticles(ParticleSystem ParticleSystem, Time DeltaTime);
        /// <summary>Call this to add particles to the particle system.</summary>
        /// <param name="ParticleSystem">Particle system to recieve the particle</param>
        /// <param name="Particle">Particle that will be added to the system</param>
        protected void EmitParticle(ParticleSystem ParticleSystem, Particle Particle)
        {
            ParticleSystem.AddParticle(Particle);
        }
    }
}
