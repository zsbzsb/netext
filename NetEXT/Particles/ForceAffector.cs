﻿using System;
using NetEXT.TimeFunctions;
using SFML.Window;
using SFML.System;

namespace NetEXT.Particles
{
    /// <summary>Applies a translational acceleration to particles over time.</summary>
    public class ForceAffector : IAffector
    {
        #region Variables
        private Vector2f _acceleration = new Vector2f(0, 0);
        #endregion

        #region Properties
        /// <summary>Gets or sets the linear acceleration applied to the particles.</summary>
        public Vector2f Acceleration
        {
            get
            {
                return _acceleration;
            }
            set
            {
                _acceleration = value;
            }
        }
        #endregion

        #region Constructors/Destructors
        /// <summary>Constructs a new force affector with the specified acceleration.</summary>
        /// <param name="Acceleration">Acceleration that will be applied to particles</param>
        public ForceAffector(Vector2f Acceleration)
        {
            _acceleration = Acceleration;
        }
        #endregion

        #region Functions
        public void ApplyAffector(Particle CurrentParticle, Time ElapsedTime)
        {
            CurrentParticle.Velocity = new Vector2f(CurrentParticle.Velocity.X + (float)(ElapsedTime.AsSeconds() * _acceleration.X), CurrentParticle.Velocity.Y + (float)(ElapsedTime.AsSeconds() * _acceleration.Y));
        }
        #endregion
    }
}
