﻿using System;
using System.Collections.Generic;
using SFML.Window;
using SFML.Graphics;
using SFML.System;

namespace NetEXT.Particles
{
    /// <summary>Class for particle systems.</summary>
    /// <remarks>A particle system stores, updates and draws particles. It also stores emitter and affector functions that control how particles are generated and modified over time. To represent particles graphically, a particle system requires a texture, and optionally one or multiple texture rectangles.</remarks>
    public class ParticleSystem : Transformable, Drawable
    {
        #region Variables
        private Texture _texture = null;
        private List<IntRect> _texturerects = new List<IntRect>();
        private List<TimeLink<ExpiringTime, EmitterBase>> _emitters = new List<TimeLink<ExpiringTime, EmitterBase>>();
        private List<TimeLink<ExpiringTime, IAffector>> _affectors = new List<TimeLink<ExpiringTime, IAffector>>();
        private List<Particle> _particles = new List<Particle>();
        private VertexArray _vertexarray = new VertexArray(PrimitiveType.Quads);
        private bool _needsvertexupdate = true;
        #endregion

        #region Properties
        /// <summary>Gets or sets the texture used by this particle system.</summary>
        public Texture Texture
        {
            get
            {
                return _texture;
            }
            set
            {
                _texture = value;
                _needsvertexupdate = true;
            }
        }
        /// <summary>Gets an array of all active emitters.</summary>
        public EmitterBase[] Emitters
        {
            get
            {
                EmitterBase[] emitters = new EmitterBase[_emitters.Count];
                for (int i = 0; i < _emitters.Count; i++) { emitters[i] = _emitters[i].Value; }
                return emitters;
            }
        }
        /// <summary>Gets an array of all active affectors.</summary>
        public IAffector[] Affectors
        {
            get
            {
                IAffector[] affectors = new IAffector[_affectors.Count];
                for (int i = 0; i < _affectors.Count; i++) { affectors[i] = _affectors[i].Value; }
                return affectors;
            }
        }
        /// <summary>Gets an array of all active particles.</summary>
        public Particle[] Particles
        {
            get
            {
                return _particles.ToArray();
            }
        }
        #endregion

        #region Constructors/Destructors
        /// <summary>Constructs a new particle system with the specified texture.</summary>
        /// <param name="Texture">Texture that will be used for this particle system</param>
        public ParticleSystem(Texture Texture)
        {
            _texture = Texture;
        }
        #endregion

        #region Functions
        /// <summary>Registers a new texture rect that can be applied to particles.</summary>
        /// <param name="TextureRect">Texture rect that should be registered</param>
        /// <returns>Index of the newly registered texture rect.</returns>
        public int AddTextureRect(IntRect TextureRect)
        {
            _texturerects.Add(TextureRect);
            return _texturerects.Count - 1;
        }
        public void Draw(RenderTarget target, RenderStates states)
        {
            UpdateVertices();
            states.Texture = null;
            if (_texture != null) states.Texture = _texture;
            states.Transform *= base.Transform;
            target.Draw(_vertexarray, states);
        }
        private void UpdateVertices()
        {
            if (!_needsvertexupdate) return;
            _needsvertexupdate = false;
            _vertexarray.Clear();
            foreach (var particle in _particles)
            {
                IntRect rect = _texturerects.Count == 0 || particle.TextureIndex >= _texturerects.Count ? new IntRect(0, 0, (int)_texture.Size.X, (int)_texture.Size.Y) : _texturerects[particle.TextureIndex];
                Vector2f topleftoffset = new Vector2f(-(rect.Width / 2), -(rect.Height / 2));
                Vector2f toprightoffset = new Vector2f((rect.Width / 2), -(rect.Height / 2));
                Vector2f bottomrightoffset = new Vector2f((rect.Width / 2), (rect.Height / 2));
                Vector2f bottomleftoffset = new Vector2f(-(rect.Width / 2), (rect.Height / 2));
                Transform transform = Transform.Identity;
                transform.Translate(particle.Position);
                transform.Rotate(particle.Rotation);
                transform.Scale(particle.Scale);
                _vertexarray.Append(new Vertex(transform.TransformPoint(topleftoffset), particle.Color, new Vector2f((float)rect.Left, (float)rect.Top)));
                _vertexarray.Append(new Vertex(transform.TransformPoint(toprightoffset), particle.Color, new Vector2f((float)rect.Left + (float)rect.Width, (float)rect.Top)));
                _vertexarray.Append(new Vertex(transform.TransformPoint(bottomrightoffset), particle.Color, new Vector2f((float)rect.Left + (float)rect.Width, (float)rect.Top + (float)rect.Height)));
                _vertexarray.Append(new Vertex(transform.TransformPoint(bottomleftoffset), particle.Color, new Vector2f((float)rect.Left, (float)rect.Top + (float)rect.Height)));
            }
        }
        /// <summary>Updates the particle system</summary>
        /// <param name="DeltaTime">The elapsed time since the last update call</param>
        public void Update(Time DeltaTime)
        {
            _needsvertexupdate = true;
            for (int i = 0; i < _emitters.Count; )
            {
                _emitters[i].Value.EmitParticles(this, DeltaTime);
                if (_emitters[i].Time.TimeUntilExpire > Time.Zero)
                {
                    _emitters[i].Time.ElapsedTime += DeltaTime;
                    if (_emitters[i].Time.ElapsedTime >= _emitters[i].Time.TimeUntilExpire)
                    {
                        _emitters.RemoveAt(i);
                    }
                    else
                    {
                        i++;
                    }
                }
                else
                {
                    i++;
                }
            }
            List<Particle> eraselist = null;
            foreach (Particle particle in _particles)
            {
                particle.ElapsedLifetime += DeltaTime;
                if (particle.ElapsedLifetime > particle.TotalLifetime)
                {
                    if (eraselist == null) eraselist = new List<Particle>();
                    eraselist.Add(particle);
                }
                else
                {
                    particle.Position = new Vector2f(particle.Position.X + (float)(DeltaTime.AsSeconds() * particle.Velocity.X), particle.Position.Y + (float)(DeltaTime.AsSeconds() * particle.Velocity.Y));
                    particle.Rotation += (float)(DeltaTime.AsSeconds() * particle.RotationSpeed);
                    foreach (TimeLink<ExpiringTime, IAffector> affector in _affectors)
                    {
                        affector.Value.ApplyAffector(particle, DeltaTime);
                    }
                }
            }
            if (eraselist != null)
            {
                foreach (Particle CurrentParticle in eraselist)
                {
                    _particles.Remove(CurrentParticle);
                }
                eraselist = null;
            }
            for (int i = 0; i < _affectors.Count; )
            {
                if (_affectors[i].Time.TimeUntilExpire > Time.Zero)
                {
                    _affectors[i].Time.ElapsedTime += DeltaTime;
                    if (_affectors[i].Time.ElapsedTime >= _affectors[i].Time.TimeUntilExpire)
                    {
                        _affectors.RemoveAt(i);
                    }
                    else
                    {
                        i++;
                    }
                }
                else
                {
                    i++;
                }
            }
        }
        /// <summary>Adds an emitter to the particle system</summary>
        /// <param name="Emitter">The emitter that will be added</param>
        public void AddEmitter(EmitterBase Emitter)
        {
            AddEmitter(Emitter, Time.Zero);
        }
        /// <summary>Adds an emitter to the particle system with the specified time to live</summary>
        /// <param name="Emitter">The emitter that will be added</param>
        /// <param name="TimeToLive">Time before the emitter will die</param>
        public void AddEmitter(EmitterBase Emitter, Time TimeToLive)
        {
            TimeLink<ExpiringTime, EmitterBase> emitter = new TimeLink<ExpiringTime, EmitterBase>(new ExpiringTime(TimeToLive), Emitter);
            if (!_emitters.Contains(emitter)) _emitters.Add(emitter);
        }
        /// <summary>Adds an affector to the particle system</summary>
        /// <param name="Affector">The affector that will be added</param>
        public void AddAffector(IAffector Affector)
        {
            AddAffector(Affector, Time.Zero);
        }
        /// <summary>Adds an affector to the particle system with the specified time to live</summary>
        /// <param name="Affector">The affector that will be added</param>
        /// <param name="TimeToLive">Time before the affector will die</param>
        public void AddAffector(IAffector Affector, Time TimeToLive)
        {
            TimeLink<ExpiringTime, IAffector> affector = new TimeLink<ExpiringTime, IAffector>(new ExpiringTime(TimeToLive), Affector);
            if (!_affectors.Contains(affector)) _affectors.Add(affector);
        }
        internal void AddParticle(Particle Particle)
        {
            _particles.Add(Particle);
            _needsvertexupdate = true;
        }
        /// <summary>Removes an emitter from the particle system</summary>
        /// <param name="Emitter">Emitter to be removed</param>
        public void RemoveEmitter(EmitterBase Emitter)
        {
            for (int i = 0; i < _emitters.Count; )
            {
                if (_emitters[i].Value == Emitter)
                {
                    _emitters.RemoveAt(i);
                }
                else
                {
                    i++;
                }
            }
        }
        /// <summary>Removes an affector from the particle system</summary>
        /// <param name="Affector">Affector to be removed</param>
        public void RemoveAffector(IAffector Affector)
        {
            for (int i = 0; i < _affectors.Count; )
            {
                if (_affectors[i].Value == Affector)
                {
                    _affectors.RemoveAt(i);
                }
                else
                {
                    i++;
                }
            }
        }
        /// <summary>Removes a particle from the particle system</summary>
        /// <param name="Particle">Particle to be removed</param>
        public void RemoveParticle(Particle Particle)
        {
            if (_particles.Contains(Particle))
            {
                _particles.Remove(Particle);
                _needsvertexupdate = true;
            }
        }
        /// <summary>Clears all currently stored emitters</summary>
        public void ClearEmitters()
        {
            _emitters.Clear();
        }
        /// <summary>Clears all currently stored affectors</summary>
        public void ClearAffectors()
        {
            _affectors.Clear();
        }
        /// <summary>Clears all currently stored particles</summary>
        public void ClearParticles()
        {
            _particles.Clear();
            _needsvertexupdate = true;
        }
        #endregion

        #region Additional Definitions
        private class ExpiringTime
        {
            #region Variables
            private Time _elapsedtime = Time.Zero;
            private Time _timeuntilexpire = Time.Zero;
            #endregion

            #region Properties
            public Time ElapsedTime
            {
                get
                {
                    return _elapsedtime;
                }
                set
                {
                    _elapsedtime = value;
                }
            }
            public Time TimeUntilExpire
            {
                get
                {
                    return _timeuntilexpire;
                }
                set
                {
                    _timeuntilexpire = value;
                }
            }
            #endregion

            #region Constructors/Destructors
            public ExpiringTime(Time NewTimeUntilExpire)
            {
                _timeuntilexpire = NewTimeUntilExpire;
            }
            #endregion

            #region Functions
            public override bool Equals(object obj)
            {
                if (((ExpiringTime)obj).TimeUntilExpire == TimeUntilExpire) return true;
                else return false;
            }
            #endregion
        }
        private class TimeLink<TimeType, ValueType>
        {
            #region Variables
            private TimeType _time;
            private ValueType _value;
            #endregion

            #region Properties
            public TimeType Time
            {
                get
                {
                    return _time;
                }
                set
                {
                    _time = value;
                }
            }
            public ValueType Value
            {
                get
                {
                    return _value;
                }
                set
                {
                    _value = value;
                }
            }
            #endregion

            #region Constructors/Destructors
            public TimeLink(TimeType NewTime, ValueType NewValue)
            {
                _time = NewTime;
                _value = NewValue;
            }
            #endregion

            #region Functions
            public override bool Equals(object obj)
            {
                if (((TimeLink<TimeType, ValueType>)obj).Value.Equals(_value)) return true;
                else return false;
            }
            #endregion
        }
        #endregion
    }
}
