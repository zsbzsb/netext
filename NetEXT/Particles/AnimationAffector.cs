﻿using System;
using SFML.Window;
using SFML.System;
using NetEXT.Animation;

namespace NetEXT.Particles
{
    /// <summary>This affector can be used to apply animations of NetEXT's Animation module to particles.</summary>
    public class AnimationAffector : IAffector
    {
        #region Variables
        private IAnimation<Particle> _animation = null;
        #endregion

        #region Properties
        /// <summary>Gets or sets the animation that will be applied to particles.</summary>
        public IAnimation<Particle> Animation
        {
            get
            {
                return _animation;
            }
            set
            {
                _animation = value;
            }
        }
        #endregion

        #region Constructors/Destructors
        /// <summary>Constructs a new animation affector with the specified animation.</summary>
        /// <param name="Animation">Animation that will be applied to particles</param>
        public AnimationAffector(IAnimation<Particle> Animation)
        {
            _animation = Animation;
        }
        #endregion

        #region Functions
        public void ApplyAffector(Particle CurrentParticle, Time ElapsedTime)
        {
            _animation.Animate(CurrentParticle, CurrentParticle.ElapsedRatio);
        }
        #endregion
    }
}
