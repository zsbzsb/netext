﻿using System;
using NetEXT.TimeFunctions;
using SFML.Window;
using SFML.System;

namespace NetEXT.Particles
{
    /// <summary>Scales particles over time.</summary>
    public class ScaleAffector : IAffector
    {
        #region Variables
        private Vector2f _scalefactor = new Vector2f(0, 0);
        #endregion

        #region Properties
        /// <summary>Gets or sets the factor by which particles are scaled every second.</summary>
        public Vector2f ScaleFactor
        {
            get
            {
                return _scalefactor;
            }
            set
            {
                _scalefactor = value;
            }
        }
        #endregion

        #region Constructors/Destructors
        /// <summary>Constructs a new scale affector with the specified scale factor.</summary>
        /// <param name="ScaleFactor">Factor by which particles will be scaled</param>
        public ScaleAffector(Vector2f ScaleFactor)
        {
            _scalefactor = ScaleFactor;
        }
        #endregion

        #region Functions
        public void ApplyAffector(Particle CurrentParticle, Time ElapsedTime)
        {
            CurrentParticle.Scale = new Vector2f(CurrentParticle.Scale.X + (float)(ElapsedTime.AsSeconds() * _scalefactor.X), CurrentParticle.Scale.Y + (float)(ElapsedTime.AsSeconds() * _scalefactor.Y));
        }
        #endregion
    }
}
