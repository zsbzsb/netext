﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using NetEXT.Utility;

namespace NetEXT.Collections
{
    /// <summary>Emulates a <see cref="System.Collections.Generic.List"/>, however an event will be raised whenever the collection is modified.</summary>
    /// <remarks>For member documentation refer to <see cref="System.Collections.Generic.List"/></remarks>
    /// <typeparam name="T">Type of elements in the list</typeparam>
    [Serializable]
    public class NotifyList<T> : NotifyChangedBase, IEnumerable
    {
        #region Variables
        private List<T> _internallist = null;
        #endregion

        #region Properties
        public int Capacity { get { return _internallist.Capacity; } set { _internallist.Capacity = value; } }
        public int Count { get { return _internallist.Count; } }
        public T this[int Index]
        {
            get
            {
                return _internallist[Index];
            }
            set
            {
                _internallist[Index] = value;
                OnNotifyChanged();
            }
        }
        #endregion

        #region Constructors
        public NotifyList()
        {
            _internallist = new List<T>();
        }
        public NotifyList(int Capacity)
        {
            _internallist = new List<T>(Capacity);
        }
        public NotifyList(IEnumerable<T> Collection)
        {
            _internallist = new List<T>(Collection);
        }
        #endregion

        #region Functions
        public void Add(T Item) { _internallist.Add(Item); OnNotifyChanged(); }
        public void AddRange(IEnumerable<T> Collection) { _internallist.AddRange(Collection); OnNotifyChanged(); }
        public ReadOnlyCollection<T> AsReadOnly() { return _internallist.AsReadOnly(); }
        public int BinarySearch(T Item) { return _internallist.BinarySearch(Item); }
        public int BinarySearch(T Item, IComparer<T> Comparer) { return _internallist.BinarySearch(Item, Comparer); }
        public int BinarySearch(int Index, int Count, T Item, IComparer<T> Comparer) { return _internallist.BinarySearch(Index, Count, Item, Comparer); }
        public void Clear() { _internallist.Clear(); OnNotifyChanged(); }
        public bool Contains(T Item) { return _internallist.Contains(Item); }
        public NotifyList<TOutput> ConvertAll<TOutput>(Converter<T, TOutput> Converter) { return new NotifyList<TOutput>(_internallist.ConvertAll<TOutput>(Converter)); }
        public void CopyTo(T[] Array) { _internallist.CopyTo(Array); }
        public void CopyTo(T[] Array, int ArrayIndex) { _internallist.CopyTo(Array, ArrayIndex); }
        public void CopyTo(int Index, T[] Array, int ArrayIndex, int Count) { _internallist.CopyTo(Index, Array, ArrayIndex, Count); }
        public bool Exists(Predicate<T> Match) { return _internallist.Exists(Match); }
        public T Find(Predicate<T> Match) { return _internallist.Find(Match); }
        public NotifyList<T> FindAll(Predicate<T> Match) { return new NotifyList<T>(_internallist.FindAll(Match)); }
        public int FindIndex(Predicate<T> Match) { return _internallist.FindIndex(Match); }
        public int FindIndex(int StartIndex, Predicate<T> Match) { return _internallist.FindIndex(StartIndex, Match); }
        public int FindIndex(int StartIndex, int Count, Predicate<T> Match) { return _internallist.FindIndex(StartIndex, Count, Match); }
        public T FindLast(Predicate<T> Match) { return _internallist.FindLast(Match); }
        public int FindLastIndex(Predicate<T> Match) { return _internallist.FindLastIndex(Match); }
        public int FindLastIndex(int StartIndex, Predicate<T> Match) { return _internallist.FindLastIndex(StartIndex, Match); }
        public int FindLastIndex(int StartIndex, int Count, Predicate<T> Match) { return _internallist.FindLastIndex(StartIndex, Count, Match); }
        public void ForEach(Action<T> Action) { _internallist.ForEach(Action); }
        public IEnumerator GetEnumerator() { return _internallist.GetEnumerator(); }
        public NotifyList<T> GetRange(int Index, int Count) { return new NotifyList<T>(_internallist.GetRange(Index, Count)); }
        public int IndexOf(T Item) { return _internallist.IndexOf(Item); }
        public int IndexOf(T Item, int Index) { return _internallist.IndexOf(Item, Index); }
        public int IndexOf(T Item, int Index, int Count) { return _internallist.IndexOf(Item, Index, Count); }
        public void Insert(int Index, T Item) { _internallist.Insert(Index, Item); OnNotifyChanged(); }
        public void InsertRange(int Index, IEnumerable<T> Collection) { _internallist.InsertRange(Index, Collection); OnNotifyChanged(); }
        public int LastIndexOf(T Item) { return _internallist.LastIndexOf(Item); }
        public int LastIndexOf(T Item, int Index) { return _internallist.LastIndexOf(Item, Index); }
        public int LastIndexOf(T Item, int Index, int Count) { return _internallist.LastIndexOf(Item, Index, Count); }
        public bool Remove(T Item) { bool result = _internallist.Remove(Item); OnNotifyChanged(); return result; }
        public int RemoveAll(Predicate<T> Match) { int result = _internallist.RemoveAll(Match); OnNotifyChanged(); return result; }
        public void RemoveAt(int Index) { _internallist.RemoveAt(Index); OnNotifyChanged(); }
        public void RemoveRange(int Index, int Count) { _internallist.RemoveRange(Index, Count); OnNotifyChanged(); }
        public void Reverse() { _internallist.Reverse(); OnNotifyChanged(); }
        public void Reverse(int Index, int Count) { _internallist.Reverse(Index, Count); OnNotifyChanged(); }
        public void Sort() { _internallist.Sort(); OnNotifyChanged(); }
        public void Sort(Comparison<T> Comparison) { _internallist.Sort(Comparison); OnNotifyChanged(); }
        public void Sort(IComparer<T> Comparer) { _internallist.Sort(Comparer); OnNotifyChanged(); }
        public void Sort(int Index, int Count, IComparer<T> Comparer) { _internallist.Sort(Index, Count, Comparer); OnNotifyChanged(); }
        public T[] ToArray() { return _internallist.ToArray(); }
        public void TrimExcess() { _internallist.TrimExcess(); }
        public bool TrueForAll(Predicate<T> Match) { return _internallist.TrueForAll(Match); }
        #endregion
    }
}
