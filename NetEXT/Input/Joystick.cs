﻿using System;

namespace NetEXT.Input
{
    /// <summary>Contains information about a joystick number and button number.</summary>
    public struct JoystickButton
    {
        public uint JoystickID;
        public uint Button;
        public JoystickButton(uint NewJoystickID, uint NewButton)
        {
            JoystickID = NewJoystickID;
            Button = NewButton;
        }
    }
    /// <summary>Contains information about a joystick number, an axis and its threshold.</summary>
    public struct JoystickAxis
    {
        public uint JoystickID;
        public SFML.Window.Joystick.Axis Axis;
        public float Threshold;
        public bool Above;
        public JoystickAxis(uint NewJoystickID, SFML.Window.Joystick.Axis NewAxis, float NewThreshold, bool NewAbove)
        {
            JoystickID = NewJoystickID;
            Axis = NewAxis;
            Threshold = NewThreshold;
            Above = NewAbove;
        }
    }
    /// <summary>Proxy class that allows the Joy(JoystickID) named parameter syntax.</summary>
    public struct Joy
    {
        public uint JoystickID;
        public Joy(uint NewJoystickID)
        {
            JoystickID = NewJoystickID;
        }
        public JoystickButton FromButton(uint Button)
        {
            return new JoystickButton(JoystickID, Button);
        }
        public Axis FromAxis(SFML.Window.Joystick.Axis Axis)
        {
            return new Axis(JoystickID, Axis);
        }
        public struct Axis
        {
            public uint JoystickID;
            public SFML.Window.Joystick.Axis JoyAxis;
            public Axis(uint NewJoystickID, SFML.Window.Joystick.Axis NewAxis)
            {
                JoystickID = NewJoystickID;
                JoyAxis = NewAxis;
            }
            public JoystickAxis Above(float Threshold)
            {
                return new JoystickAxis(JoystickID, JoyAxis, Threshold, true);
            }
            public JoystickAxis Below(float Threshold)
            {
                return new JoystickAxis(JoystickID, JoyAxis, Threshold, false);
            }
        }
    }
}
