﻿using System;
using System.Collections.Generic;
using SFML.Window;
using NetEXT.Utility;

namespace NetEXT.Input
{
    /// <summary>Class for connecting identifiers with sequence of callbacks.</summary>
    /// <typeparam name="T">Type of identifier to associate with callbacks</typeparam>
    /// <typeparam name="U">Type of event that will be passed as a parameter to callbacks</typeparam>
    public class EventSystem<T, U>
    {
        #region Variables
        private Dictionary<T, CallbackSequence<Action<U>>> _callbacks = new Dictionary<T, CallbackSequence<Action<U>>>();
        private Func<U, T> _geteventid = new Func<U, T>((U inobject) => { dynamic obj = inobject; return (T)obj; });
        #endregion

        #region Properties
        public Func<U, T> GetEventIDHandler
        {
            get
            {
                return _geteventid;
            }
            set
            {
                _geteventid = value;
            }
        }
        #endregion

        #region Functions
        /// <summary>Calls all listener functions that are currently associated with an event.</summary>
        /// <param name="Event">Event to pass to callbacks</param>
        public void TriggerEvent(U Event)
        {
            if (_geteventid == null) return;
            T id = _geteventid(Event);
            if (!_callbacks.ContainsKey(id)) return;
            _callbacks[id].InvokeCallbacks(Event);
        }
        /// <summary>Clears callbacks that are associated with the specified ID</summary>
        /// <param name="ID">Identifier that is associated with callbacks</param>
        public void ClearConnections(T ID)
        {
            if (!_callbacks.ContainsKey(ID)) return;
            _callbacks.Remove(ID);
        }
        /// <summary>Clears all callbacks that are associated with all IDs</summary>
        public void ClearAllConnections()
        {
            _callbacks.Clear();
        }
        /// <summary>Associates a callback with an ID</summary>
        /// <param name="ID">Identifier to associate the callback with</param>
        /// <param name="Callback">Callback to register with the specified ID</param>
        public void Connect(T ID, Action<U> Callback)
        {
            if (!_callbacks.ContainsKey(ID)) _callbacks.Add(ID, new CallbackSequence<Action<U>>());
            _callbacks[ID].RegisterCallback(Callback);
        }
        /// <summary>Removes a callback that was previously associated with an ID</summary>
        /// <param name="ID">Identifier that the callback is associated with</param>
        /// <param name="Callback">Callback to remove</param>
        public void Diconnect(T ID, Action<U> Callback)
        {
            if (!_callbacks.ContainsKey(ID)) return;
            _callbacks[ID].UnregisterCallback(Callback);
        }
        #endregion
    }
}
