﻿using System;
using SFML.Window;

namespace NetEXT.Input
{
    /// <summary>Structure containing information about the context in which an action has occurred.</summary>
    /// <typeparam name="T">Type of the action ID</typeparam>
    public struct ActionContext<T>
    {
        /// <summary>Window that passed to ActionMap.InvokeCallbacks(...)</summary>
        /// <remarks>Use this variable to access the window inside a callback function. This variable can be null if you didn't specify a window when calling ActionMap.InvokeCallbacks(...)</remarks>
        public Window Window;
        /// <summary>Event that contributed to this action's activation.</summary>
        /// <remarks>Do not store the event. It is null if the action hasn't been triggered by an event.</remarks>
        public Event? Event;
        /// <summary>Identifier of the action.</summary>
        public T ActionID;
        public ActionContext(Window NewWindow, Event? NewEvent, T NewActionID)
        {
            Window = NewWindow;
            Event = NewEvent;
            ActionID = NewActionID;
        }
    }
}
