﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetEXT.MathFunctions
{
    /// <summary>Class holding a rule to create values with predefined properties.</summary>
    /// <typeparam name="T">Type of value to return</typeparam>
    public class Distribution<T>
    {
        #region Variables
        private Func<T> _callback = null;
        private T _value;
        #endregion

        #region Constructors/Destructors
        /// <summary>Creates a distribution with the specified value.</summary>
        /// <param name="Value">Value to be returned from this distribution</param>
        public Distribution(T Value)
        {
            _value = Value;
        }
        /// <summary>Creates a distribution with the specified value.</summary>
        /// <param name="Callback">Callback function that will return a value that is returned from this distribution</param>
        public Distribution(Func<T> Callback)
        {
            _callback = Callback;
        }
        #endregion

        #region Operators
        public static implicit operator Distribution<T>(T Value)
        {
            return new Distribution<T>(Value);
        }
        public static implicit operator Distribution<T>(Func<T> Callback)
        {
            return new Distribution<T>(Callback);
        }
        public static implicit operator T(Distribution<T> Distribution)
        {
            if (Distribution._callback != null) return Distribution._callback();
            else return Distribution._value;
        }
        #endregion
    }
}
