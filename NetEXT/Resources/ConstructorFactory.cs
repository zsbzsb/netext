﻿using System;
using System.IO;
using SFML.Window;
using SFML.Graphics;
using SFML.Audio;
using SFML.System;
using NetEXT.Utility;

namespace NetEXT.Resources
{
    /// <summary>Factory class to return dynamic delegates populated with all the normal constructors.</summary>
    public static class ConstructorFactory
    {
        /// <summary>Returns a dynamic delegate that can construct textures with the normal overloads.</summary>
        public static DynamicDelegate GenerateTextureConstructors()
        {
            DynamicDelegate constructors = new DynamicDelegate();
            constructors.RegisterDelegate(new Func<Image, Texture>((img) => { return new Texture(img); }));
            constructors.RegisterDelegate(new Func<Stream, Texture>((stream) => { return new Texture(stream); }));
            constructors.RegisterDelegate(new Func<string, Texture>((filename) => { return new Texture(filename); }));
            constructors.RegisterDelegate(new Func<Image, IntRect, Texture>((img, area) => { return new Texture(img, area); }));
            constructors.RegisterDelegate(new Func<Stream, IntRect, Texture>((stream, area) => { return new Texture(stream, area); }));
            constructors.RegisterDelegate(new Func<string, IntRect, Texture>((filename, area) => { return new Texture(filename, area); }));
            constructors.RegisterDelegate(new Func<uint, uint, Texture>((width, height) => { return new Texture(width, height); }));
            return constructors;
        }
        /// <summary>Returns a dynamic delegate that can construct images with the normal overloads.</summary>
        public static DynamicDelegate GenerateImageConstructors()
        {
            DynamicDelegate constructors = new DynamicDelegate();
            constructors.RegisterDelegate(new Func<Stream, Image>((stream) => { return new Image(stream); }));
            constructors.RegisterDelegate(new Func<string, Image>((filename) => { return new Image(filename); }));
            constructors.RegisterDelegate(new Func<uint, uint, Image>((width, height) => { return new Image(width, height); }));
            constructors.RegisterDelegate(new Func<uint, uint, Color, Image>((width, height, color) => { return new Image(width, height, color); }));
            constructors.RegisterDelegate(new Func<uint, uint, byte[], Image>((width, height, pixels) => { return new Image(width, height, pixels); }));
            constructors.RegisterDelegate(new Func<Color[,], Image>((pixels) => { return new Image(pixels); }));
            return constructors;
        }
        /// <summary>Returns a dynamic delegate that can construct fonts with the normal overloads.</summary>
        public static DynamicDelegate GenerateFontConstructors()
        {
            DynamicDelegate constructors = new DynamicDelegate();
            constructors.RegisterDelegate(new Func<Stream, Font>((stream) => { return new Font(stream); }));
            constructors.RegisterDelegate(new Func<string, Font>((filename) => { return new Font(filename); }));
            return constructors;
        }
        /// <summary>Returns a dynamic delegate that can construct shaders with the normal overloads.</summary>
        public static DynamicDelegate GenerateShaderConstructors()
        {
            DynamicDelegate constructors = new DynamicDelegate();
            constructors.RegisterDelegate(new Func<IntPtr, Shader>((ptr) => { return new Shader(ptr); }));
            constructors.RegisterDelegate(new Func<Stream, Stream, Shader>((vertstream, fragstream) => { return new Shader(vertstream, fragstream); }));
            constructors.RegisterDelegate(new Func<string, string, Shader>((vertfilename, framefilename) => { return new Shader(vertfilename, framefilename); }));
            return constructors;
        }
        /// <summary>Returns a dynamic delegate that can construct sound buffers with the normal overloads.</summary>
        public static DynamicDelegate GenerateSoundBufferConstructors()
        {
            DynamicDelegate constructors = new DynamicDelegate();
            constructors.RegisterDelegate(new Func<short[], uint, uint, SoundBuffer>((samples, channelcount, samplerate) => { return new SoundBuffer(samples, channelcount, samplerate); }));
            constructors.RegisterDelegate(new Func<Stream, SoundBuffer>((stream) => { return new SoundBuffer(stream); }));
            constructors.RegisterDelegate(new Func<string, SoundBuffer>((filename) => { return new SoundBuffer(filename); }));
            return constructors;
        }
    }
}
