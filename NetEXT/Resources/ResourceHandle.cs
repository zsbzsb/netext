﻿using System;

namespace NetEXT.Resources
{
    /// <summary>Provides a way to access stored resources.</summary>
    /// <remarks>You must keep the handle alive as long as you are using the resource, or else you risk the resource being disposed while it is still in use.</remarks>
    /// <typeparam name="T">Type of resource</typeparam>
    /// <typeparam name="U">Type of resource identifier</typeparam>
    public class ResourceHandle<T, U> where T : IDisposable
    {
        #region Variables
        private ResourceHolder<T, U> _resource = null;
        private bool _released = false;
        #endregion

        #region Properties
        /// <summary>Gets the resource that this handle is pointing at.</summary>
        public T Resource
        {
            get
            {
                if (_released) throw new Exception("The handle has already been released");
                return _resource.InternalResource;
            }
        }
        #endregion

        #region Constructors/Destructors
        internal ResourceHandle(ResourceHolder<T, U> Resource)
        {
            _resource = Resource;
            _resource.ReferenceCount += 1;
        }
        ~ResourceHandle()
        {
            ReleaseHandle();
        }
        /// <summary>Releases and invalidates this handle.</summary>
        /// <remarks>After a handle has been released you must aquire a new handle to continue using the resource.</remarks>
        public void ReleaseHandle()
        {
            if (!_released) _resource.ReferenceCount -= 1;
            _released = true;
        }
        #endregion

        #region Operators
        public static implicit operator T(ResourceHandle<T, U> Handle)
        {
            return Handle.Resource;
        }
        #endregion
    }
}
