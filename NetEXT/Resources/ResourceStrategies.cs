﻿using System;

namespace NetEXT.Resources
{
    /// <summary>Determines how caches manage resource lifetimes.</summary>
    public enum ResourceStrategies
    {
        /// <summary>Automatically release resources as soon as they are no longer referenced.</summary>
        AutoRelease,
        /// <summary>Resources will be kept in memory until they are explicitly released</summary>
        ExplicitRelease
    }
}
