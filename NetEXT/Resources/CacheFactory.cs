﻿using System;
using SFML.Window;
using SFML.Graphics;
using SFML.Audio;
using SFML.System;

namespace NetEXT.Resources
{
    /// <summary>Factory class to construct resource caches with the default contructors.</summary>
    public static class CacheFactory
    {
        /// <summary>Constructs a texture cache with the default constructors.</summary>
        /// <typeparam name="U">Type that will be used to identify resources</typeparam>
        /// <param name="ResourceStrategy">Strategy that will be used by the returned cache</param>
        public static ResourceCache<Texture, U> CreateTextureCache<U>(ResourceStrategies ResourceStrategy = ResourceStrategies.AutoRelease)
        {
            return new ResourceCache<Texture, U>(ConstructorFactory.GenerateTextureConstructors(), ResourceStrategy);
        }
        /// <summary>Constructs an image cache with the default constructors.</summary>
        /// <typeparam name="U">Type that will be used to identify resources</typeparam>
        /// <param name="ResourceStrategy">Strategy that will be used by the returned cache</param>
        public static ResourceCache<Image, U> CreateImageCache<U>(ResourceStrategies ResourceStrategy = ResourceStrategies.AutoRelease)
        {
            return new ResourceCache<Image, U>(ConstructorFactory.GenerateImageConstructors(), ResourceStrategy);
        }
        /// <summary>Constructs a font cache with the default constructors.</summary>
        /// <typeparam name="U">Type that will be used to identify resources</typeparam>
        /// <param name="ResourceStrategy">Strategy that will be used by the returned cache</param>
        public static ResourceCache<Font, U> CreateFontCache<U>(ResourceStrategies ResourceStrategy = ResourceStrategies.AutoRelease)
        {
            return new ResourceCache<Font, U>(ConstructorFactory.GenerateFontConstructors(), ResourceStrategy);
        }
        /// <summary>Constructs a shader cache with the default constructors.</summary>
        /// <typeparam name="U">Type that will be used to identify resources</typeparam>
        /// <param name="ResourceStrategy">Strategy that will be used by the returned cache</param>
        public static ResourceCache<Shader, U> CreateShaderCache<U>(ResourceStrategies ResourceStrategy = ResourceStrategies.AutoRelease)
        {
            return new ResourceCache<Shader, U>(ConstructorFactory.GenerateShaderConstructors(), ResourceStrategy);
        }
        /// <summary>Constructs a sound buffer cache with the default constructors.</summary>
        /// <typeparam name="U">Type that will be used to identify resources</typeparam>
        /// <param name="ResourceStrategy">Strategy that will be used by the returned cache</param>
        public static ResourceCache<SoundBuffer, U> CreateSoundBufferCache<U>(ResourceStrategies ResourceStrategy = ResourceStrategies.AutoRelease)
        {
            return new ResourceCache<SoundBuffer, U>(ConstructorFactory.GenerateSoundBufferConstructors(), ResourceStrategy);
        }
        /// <summary>Constructs a resource cache that can store all resource types with the default constructors for textures, images, fonts, shaders, and sound buffers.</summary>
        /// <typeparam name="U">Type that will be used to identify resources</typeparam>
        /// <param name="ResourceStrategy">Strategy that will be used by the returned cache</param>
        public static MultiResourceCache<U> CreateMultiResourceCache<U>(ResourceStrategies ResourceStrategy = ResourceStrategies.AutoRelease)
        {
            return new MultiResourceCache<U>(ConstructorFactory.GenerateTextureConstructors() + ConstructorFactory.GenerateImageConstructors() + ConstructorFactory.GenerateFontConstructors() + ConstructorFactory.GenerateShaderConstructors() + ConstructorFactory.GenerateSoundBufferConstructors(), ResourceStrategy);
        }
    }
}
