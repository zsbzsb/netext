﻿using System;
using System.Threading;

namespace NetEXT.Concurrent
{
    /// <summary>A nice wrapper around a mutex that allows a thread to lock an object.</summary>
    public abstract class Lockable
    {
        #region Variables
        private Mutex _lockmutex = new Mutex(false);
        #endregion

        #region Functions
        /// <summary>Blocking call that will lock the current object for the current thread.</summary>
        public void Lock()
        {
            _lockmutex.WaitOne();
        }
        /// <summary>Releases a lock that a thread has on the current object.</summary>
        public void ReleaseLock()
        {
            _lockmutex.ReleaseMutex();
        }
        #endregion
    }
}
