﻿using System;
using System.Collections.Generic;
using SFML.Window;
using SFML.Graphics;
using SFML.System;

namespace NetEXT.Graphics
{
    /// <summary>Class for drawing the <see cref="NetEXT.Graphics.LargeTexture"/>.</summary>
    public class LargeSprite : Transformable, Drawable
    {
        #region Variables
        private LargeTexture _currenttexture = null;
        private Sprite[] _spritelist = new Sprite[0];
        private Color _spritecolor = Color.White;
        #endregion

        #region Properties
        /// <summary>Gets or sets the color of the sprite.</summary>
        public Color Color
        {
            get
            {
                return _spritecolor;
            }
            set
            {
                _spritecolor = value;
                for (int i = 0; i < _spritelist.Length; i++)
                {
                    _spritelist[i].Color = _spritecolor;
                }
            }
        }
        /// <summary>Gets or sets the texture.</summary>
        public LargeTexture Texture
        {
            get
            {
                return _currenttexture;
            }
            set
            {
                _currenttexture = value;
                UpdateSpriteList();
            }
        }
        #endregion

        #region Constructors/Destructors
        /// <summary>Creates a new LargeSprite with the specified texture.</summary>
        /// <param name="Texture">Texture that will be used for this sprite</param>
        public LargeSprite(LargeTexture Texture)
        {
            _currenttexture = Texture;
            UpdateSpriteList();
        }
        #endregion

        #region Functions
        private void UpdateSpriteList()
        {
            for (int i = 0; i < _spritelist.Length; i++)
            {
                _spritelist[i].Dispose();
                _spritelist[i] = null;
            }
            _spritelist = new Sprite[_currenttexture.TextureList.Length];
            for (int i = 0; i < _spritelist.Length; i++)
            {
                _spritelist[i] = new Sprite(_currenttexture.TextureList[i]);
                _spritelist[i].Position = new Vector2f(_currenttexture.PositionList[i].X, _currenttexture.PositionList[i].Y);
                _spritelist[i].Color = _spritecolor;
            }
        }
        public void Draw(RenderTarget target, RenderStates states)
        {
            states.Transform *= base.Transform;
            for (int i = 0; i < _spritelist.Length; i++)
            {
                target.Draw(_spritelist[i], states);
            }
        }
        /// <summary>Not currently implemented.</summary>
        public FloatRect GetLocalBounds()
        {
            throw new Exception("Not currently implemented");
        }
        /// <summary>Not currently implemented.</summary>
        public FloatRect GetGlobalBounds()
        {
            throw new Exception("Not currently implemented");
        }
        #endregion
    }
}
