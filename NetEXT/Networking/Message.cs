﻿using System;
using System.Text;
using System.Collections.Generic;

namespace NetEXT.Networking
{
    public class Message
    {
        #region Variables
        private List<byte> _databuffer = null;
        private int _position = 0;
        #endregion

        #region Properties
        public long Size
        {
            get
            {
                return _databuffer.Count;
            }
        }
        public int Position
        {
            get
            {
                return _position;
            }
            set
            {
                _position = value;
            }
        }
        public byte[] Buffer
        {
            get
            {
                return _databuffer.ToArray();
            }
        }
        #endregion

        #region Constructors
        public Message()
        {
            _databuffer = new List<byte>();
        }
        public Message(byte[] Bytes)
        {
            _databuffer = new List<byte>(Bytes);
        }
        #endregion

        #region Functions
        public void Clear()
        {
            _databuffer.Clear();
            _position = 0;
        }
        public void WriteBytes(byte[] Bytes)
        {
            if (BitConverter.IsLittleEndian) Array.Reverse(Bytes);
            _databuffer.InsertRange(_position, Bytes);
            _position += Bytes.Length;
        }
        public byte[] ReadBytes(int Length)
        {
            byte[] bytes = _databuffer.GetRange(_position, Length).ToArray();
            _position += Length;
            if (BitConverter.IsLittleEndian) Array.Reverse(bytes);
            return bytes;
        }
        #region Write Functions
        public void WriteByte(byte Value)
        {
            WriteBytes(new byte[] { Value });
        }
        public void WriteShort(short Value)
        {
            WriteBytes(BitConverter.GetBytes(Value));
        }
        public void WriteInt(int Value)
        {
            WriteBytes(BitConverter.GetBytes(Value));
        }
        public void WriteLong(long Value)
        {
            WriteBytes(BitConverter.GetBytes(Value));
        }
        public void WriteUShort(ushort Value)
        {
            WriteBytes(BitConverter.GetBytes(Value));
        }
        public void WriteUInt(uint Value)
        {
            WriteBytes(BitConverter.GetBytes(Value));
        }
        public void WriteULong(ulong Value)
        {
            WriteBytes(BitConverter.GetBytes(Value));
        }
        public void WriteBool(bool Value)
        {
            WriteBytes(BitConverter.GetBytes(Value));
        }
        public void WriteFloat(float Value)
        {
            WriteBytes(BitConverter.GetBytes(Value));
        }
        public void WriteDouble(double Value)
        {
            WriteBytes(BitConverter.GetBytes(Value));
        }
        public void WriteChar(char Value)
        {
            WriteBytes(BitConverter.GetBytes(Value));
        }
        public void WriteString(string Value)
        {
            WriteInt(Value.Length);
            foreach (char character in Value.ToCharArray())
            {
                WriteChar(character);
            }
        }
        #endregion
        #region Read Functions
        public byte ReadByte()
        {
            return ReadBytes(1)[0];
        }
        public short ReadShort()
        {
            return BitConverter.ToInt16(ReadBytes(2), 0);
        }
        public int ReadInt()
        {
            return BitConverter.ToInt32(ReadBytes(4), 0);
        }
        public long ReadLong()
        {
            return BitConverter.ToInt64(ReadBytes(8), 0);
        }
        public ushort ReadUShort()
        {
            return BitConverter.ToUInt16(ReadBytes(2), 0);
        }
        public uint ReadUInt()
        {
            return BitConverter.ToUInt32(ReadBytes(4), 0);
        }
        public ulong ReadULong()
        {
            return BitConverter.ToUInt64(ReadBytes(8), 0);
        }
        public bool ReadBool()
        {
            return BitConverter.ToBoolean(ReadBytes(1), 0);
        }
        public float ReadFloat()
        {
            return BitConverter.ToSingle(ReadBytes(4), 0);
        }
        public double ReadDouble()
        {
            return BitConverter.ToDouble(ReadBytes(8), 0);
        }
        public char ReadChar()
        {
            return BitConverter.ToChar(ReadBytes(2), 0);
        }
        public string ReadString()
        {
            int length = ReadInt();
            string retval = "";
            for (int i = 0; i < length; i++)
            {
                retval += ReadChar();
            }
            return retval;
        }
        #endregion
        #endregion
    }
}
