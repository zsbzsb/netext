﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using NetEXT.Concurrent;

namespace NetEXT.Networking
{
    public class TCPSocket : ReliableTransport
    {
        #region Variables
        private TcpClient _client = null;
        private int _port = 0;
        private IPAddress _address = null;
        private string _host = "";
        private bool _connected = false;
        private byte[] _receivebuffer = new byte[2048];
        private List<byte> _sendingbuffer = new List<byte>();
        private List<byte> _receivingbuffer = new List<byte>();
        private bool _issending = false;
        private Dispatcher _dispatcher = null;
        #endregion

        #region Properties
        public override int SendingBufferLength
        {
            get
            {
                return _sendingbuffer.Count;
            }
        }
        public override int ReceivingBufferLength
        {
            get
            {
                return _receivingbuffer.Count;
            }
        }
        public override bool IsConnected
        {
            get
            {
                return _connected;
            }
        }
        #endregion

        #region Constructors
        public TCPSocket(int Port, IPAddress Address, Dispatcher CurrentDispatcher = null)
        {
            _client = new TcpClient();
            _port = Port;
            _address = Address;
            if (CurrentDispatcher == null) _dispatcher = Dispatcher.CurrentDispatcher;
            else _dispatcher = CurrentDispatcher;
        }
        public TCPSocket(int Port, string Host, Dispatcher CurrentDispatcher = null)
        {
            _client = new TcpClient();
            _port = Port;
            _host = Host;
            if (CurrentDispatcher == null) _dispatcher = Dispatcher.CurrentDispatcher;
            else _dispatcher = CurrentDispatcher;
        }
        internal TCPSocket(TcpClient Client, Dispatcher CurrentDispatcher)
        {
            _client = Client;
            _connected = true;
            _client.NoDelay = true;
            _dispatcher = CurrentDispatcher;
            _client.GetStream().BeginRead(_receivebuffer, 0, _receivebuffer.Length, EndRecieve, null);
            _dispatcher.InvokeAsync((Action)OnConnected);
        }
        #endregion

        #region Functions
        public override void Connect()
        {
            if (_connected) return;
            try
            {
                if (_address != null) _client.BeginConnect(_address, _port, new AsyncCallback(EndConnect), null);
                else _client.BeginConnect(_host, _port, new AsyncCallback(EndConnect), null);
            }
            catch
            {
                _connected = false;
                _dispatcher.InvokeAsync((Action)OnDisconnected);
            }
        }
        private void EndConnect(IAsyncResult Result)
        {
            try
            {
                _client.EndConnect(Result);
                _connected = true;
                _client.NoDelay = true;
                _client.GetStream().BeginRead(_receivebuffer, 0, _receivebuffer.Length, EndRecieve, null);
                _dispatcher.InvokeAsync((Action)OnConnected);
            }
            catch
            {
                _connected = false;
                _dispatcher.InvokeAsync((Action)OnDisconnected);
            }
        }
        private void EndRecieve(IAsyncResult Result)
        {
            if (_connected)
            {
                try
                {
                    int length = _client.GetStream().EndRead(Result);
                    if (length <= 0)
                    {
                        Disconnect();
                        _dispatcher.InvokeAsync((Action)OnDisconnected);
                    }
                    else
                    {
                        Lock();
                        byte[] newdata = new byte[length];
                        Array.Copy(_receivebuffer, newdata, newdata.Length);
                        _receivingbuffer.AddRange(newdata);
                        ReleaseLock();
                        _client.GetStream().BeginRead(_receivebuffer, 0, _receivebuffer.Length, EndRecieve, null);
                        _dispatcher.InvokeAsync((Action)OnDataRecieved);
                    }
                }
                catch
                {
                    Disconnect();
                    _dispatcher.InvokeAsync((Action)OnDisconnected);
                }
            }
        }
        public override void SendData(byte[] Data)
        {
            if (_connected)
            {
                if (_issending)
                {
                    _sendingbuffer.AddRange(Data);
                }
                else
                {
                    _issending = true;
                    _sendingbuffer.AddRange(Data);
                    byte[] senddata = new byte[Math.Min(2048, _sendingbuffer.Count)];
                    Array.Copy(_sendingbuffer.ToArray(), senddata, senddata.Length);
                    _sendingbuffer.RemoveRange(0, senddata.Length);
                    _client.GetStream().BeginWrite(senddata, 0, senddata.Length, EndWrite, null);
                }
            }
        }
        private void EndWrite(IAsyncResult Result)
        {
            if (_connected)
            {
                _client.GetStream().EndWrite(Result);
                Lock();
                if (_sendingbuffer.Count > 0)
                {
                    byte[] senddata = new byte[Math.Min(2048, _sendingbuffer.Count)];
                    Array.Copy(_sendingbuffer.ToArray(), senddata, senddata.Length);
                    _sendingbuffer.RemoveRange(0, senddata.Length);
                    _client.GetStream().BeginWrite(senddata, 0, senddata.Length, EndWrite, null);
                }
                else
                {
                    _issending = false;
                }
                ReleaseLock();
            }
            else
            {
                _issending = false;
            }
        }
        public override void Disconnect()
        {
            _connected = false;
            _issending = false;
            _client.Close();
            _client = new TcpClient();
            ClearBuffers();
        }
        public override void Shutdown()
        {
            _connected = false;
            _client.Close();
            ClearBuffers();
        }
        public override void ClearBuffers()
        {
            _sendingbuffer.Clear();
            _receivingbuffer.Clear();
        }
        public override byte[] PeakReceivingBuffer(int Length)
        {
            byte[] result = new byte[Math.Min(Length, _receivingbuffer.Count)];
            Array.Copy(_receivingbuffer.ToArray(), result, result.Length);
            return result;
        }
        public override byte[] HandleReceivingBuffer(int Length)
        {
            byte[] result = new byte[Math.Min(Length, _receivingbuffer.Count)];
            Array.Copy(_receivingbuffer.ToArray(), result, result.Length);
            _receivingbuffer.RemoveRange(0, result.Length);
            return result;
        }
        #endregion
    }
}
