﻿using System;
using NetEXT.TimeFunctions;

namespace NetEXT.Networking
{
    public abstract class SyncedTypeBase
    {
        #region Variables
        private SyncedObject _owner = null;
        private SynchronizationType _synctype = SynchronizationType.Static;
        #endregion

        #region Properties
        public SynchronizationType SynchronizationType
        {
            get
            {
                return _synctype;
            }
        }
        #endregion

        #region Events
        internal event Action NotifyChanged;
        #endregion

        #region Constructors
        protected SyncedTypeBase(SyncedObject Owner, SynchronizationType SynchronizationType)
        {
            _owner = Owner;
            _synctype = SynchronizationType;
            _owner.RegisterMemeber(this);
        }
        #endregion

        #region Functions
        internal void Serialize(Message Message, SynchronizationType SynchronizationType)
        {
            if ((int)_synctype >= (int)SynchronizationType)
            {
                OnSerialize(Message);
            }
        }
        protected abstract void OnSerialize(Message Message);
        internal void Deserialize(Message Message, SynchronizationType SynchronizationType)
        {
            if ((int)_synctype >= (int)SynchronizationType)
            {
                OnDeserialize(Message);
            }
        }
        protected abstract void OnDeserialize(Message Message);
        protected void OnNotifyChanged()
        {
            if (NotifyChanged != null) NotifyChanged();
        }
        #endregion
    }
}
