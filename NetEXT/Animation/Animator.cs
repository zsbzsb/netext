﻿using System;
using System.Collections.Generic;
using SFML.Window;
using SFML.System;

namespace NetEXT.Animation
{
    /// <summary>Class that stores the progress of an object's animation.</summary>
    /// <remarks>The Animator class takes care of multiple animations which are registered by an ID. The animations can be played at any time. Animator updates their progress and applies it to animated objects.</remarks>
    /// <typeparam name="T">The type of object to animate</typeparam>
    /// <typeparam name="U">The type of the animation id</typeparam>
    public class Animator<T, U>
    {
        #region Variables
        private Dictionary<U, IAnimation<T>> _animationlist = new Dictionary<U, IAnimation<T>>();
        private Dictionary<U, Time> _animationdurationlist = new Dictionary<U, Time>();
        private Dictionary<U, Time> _playinganimationdurationlist = new Dictionary<U, Time>();
        private Dictionary<U, bool> _playinganimationlooplist = new Dictionary<U, bool>();
        #endregion

        #region Properties
        /// <summary>Gets a list of all currently playing animations by the animation ID.</summary>
        public U[] AnimationsPlaying
        {
            get
            {
                U[] keys = new U[_playinganimationdurationlist.Keys.Count];
                _playinganimationdurationlist.Keys.CopyTo(keys, 0);
                return keys;
            }
        }
        #endregion

        #region Functions
        /// <summary>Registers an animation for use.</summary>
        /// <param name="AnimationID">A unique id for this animation</param>
        /// <param name="Animation">The animation to register</param>
        /// <param name="Duration">Length of the animation</param>
        public void AddAnimation(U AnimationID, IAnimation<T> Animation, Time Duration)
        {
            if (_animationlist.ContainsKey(AnimationID)) return;
            _animationlist.Add(AnimationID, Animation);
            _animationdurationlist.Add(AnimationID, Duration);
        }
        /// <summary>Unregisters an animation.</summary>
        /// <param name="AnimationID">ID of the animation to unregister</param>
        public void RemoveAnimation(U AnimationID)
        {
            if (_animationlist.ContainsKey(AnimationID))
            {
                _animationlist.Remove(AnimationID);
                _animationdurationlist.Remove(AnimationID);
            }
            if (_playinganimationdurationlist.ContainsKey(AnimationID))
            {
                _playinganimationdurationlist.Remove(AnimationID);
                _playinganimationlooplist.Remove(AnimationID);
            }
        }
        /// <summary>Begins playing an animation.</summary>
        /// <param name="AnimationID">ID of the animation to play</param>
        public void PlayAnimation(U AnimationID)
        {
            PlayAnimation(AnimationID, false, true);
        }
        /// <summary>Begins playing an animation.</summary>
        /// <param name="AnimationID">ID of the animation to play</param>
        /// <param name="LoopAnimation">A flag to determine if the animation should be looped</param>
        public void PlayAnimation(U AnimationID, bool LoopAnimation)
        {
            PlayAnimation(AnimationID, LoopAnimation, true);
        }
        /// <summary>Begins playing an animation.</summary>
        /// <param name="AnimationID">ID of the animation to play</param>
        /// <param name="LoopAnimation">Flag to determine if the animation should be looped</param>
        /// <param name="StopOtherAnimations">Flag to determine if other playing animations should be stopped</param>
        public void PlayAnimation(U AnimationID, bool LoopAnimation, bool StopOtherAnimations)
        {
            if (StopOtherAnimations)
            {
                _playinganimationdurationlist.Clear();
                _playinganimationlooplist.Clear();
            }
            if (!_animationlist.ContainsKey(AnimationID)) return;
            if (_playinganimationdurationlist.ContainsKey(AnimationID))
            {
                _playinganimationdurationlist[AnimationID] = Time.Zero;
                _playinganimationlooplist[AnimationID] = LoopAnimation;
            }
            else
            {
                _playinganimationdurationlist.Add(AnimationID, Time.Zero);
                _playinganimationlooplist.Add(AnimationID, LoopAnimation);
            }
        }
        /// <summary>Stops a currently playing animation.</summary>
        /// <param name="AnimationID">ID of the animation to stop</param>
        public void StopAnimation(U AnimationID)
        {
            if (!_playinganimationdurationlist.ContainsKey(AnimationID)) return;
            _playinganimationdurationlist.Remove(AnimationID);
            _playinganimationlooplist.Remove(AnimationID);
        }
        /// <summary>Stops all currently playing animations.</summary>
        public void StopAllAnimations()
        {
            foreach (U id in AnimationsPlaying)
            {
                StopAnimation(id);
            }
        }
        /// <summary>Updates the currently playing animations.</summary>
        /// <param name="DeltaTime">Elapsed time since the last update call</param>
        public void Update(Time DeltaTime)
        {
            List<object> eraselist = null;
            foreach (U id in AnimationsPlaying)
            {
                _playinganimationdurationlist[id] += DeltaTime;
                if (_playinganimationdurationlist[id] > _animationdurationlist[id])
                {
                    if (_playinganimationlooplist[id])
                    {
                        while (_playinganimationdurationlist[id] > _animationdurationlist[id])
                        {
                            _playinganimationdurationlist[id] -= _animationdurationlist[id];
                        }
                    }
                    else
                    {
                        if (eraselist == null) eraselist = new List<object>();
                        eraselist.Add(id);
                    }
                }
            }
            if (eraselist != null)
            {
                foreach (U id in eraselist)
                {
                    StopAnimation(id);
                }
            }
        }
        /// <summary>Applies the playing animations to the object.</summary>
        /// <param name="AnimatedObject">Object to apply the animations to</param>
        public void Animate(AnimatedObject<T> AnimatedObject)
        {
            foreach (U id in _playinganimationdurationlist.Keys)
            {
                _animationlist[id].Animate(AnimatedObject, (float)(_playinganimationdurationlist[id].AsSeconds() / _animationdurationlist[id].AsSeconds()));
            }
        }
        #endregion
    }
}