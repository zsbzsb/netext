﻿using System;
using SFML.Window;
using SFML.Graphics;
using SFML.System;

namespace NetEXT.Animation
{
    /// <summary>Class to provide duck typing to allow arbitrary objects to be animated.</summary>
    /// <typeparam name="T">The type of object to animate.</typeparam>
    public class AnimatedObject<T>
    {
        #region Variables
        private dynamic _object = default(T);
        #endregion

        #region Properties
        public IntRect TextureRect
        {
            get
            {
                return _object.TextureRect;
            }
            set
            {
                _object.TextureRect = value;
            }
        }
        public Color Color
        {
            get
            {
                return _object.Color;
            }
            set
            {
                _object.Color = value;
            }
        }
        public Vector2f Position
        {
            get
            {
                return _object.Position;
            }
            set
            {
                _object.Position = value;
            }
        }
        public float Rotation
        {
            get
            {
                return _object.Rotation;
            }
            set
            {
                _object.Rotation = value;
            }
        }
        public Vector2f Scale
        {
            get
            {
                return _object.Scale;
            }
            set
            {
                _object.Scale = value;
            }
        }
        #endregion

        #region Constructors
        public AnimatedObject(T Object)
        {
            _object = Object;
        }
        #endregion

        #region Operators
        public static implicit operator AnimatedObject<T>(T Object)
        {
            return new AnimatedObject<T>(Object);
        }
        #endregion
    }
}
