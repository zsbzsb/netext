﻿using System;
using SFML.Window;
using SFML.Graphics;
using NetEXT.TimeFunctions;
using NetEXT.Particles;
using NetEXT.Vectors;

namespace NetEXT.Examples
{
    public class Fireworks_Example
    {
        #region Consts
        public static readonly Time ExplosionsInterval = Time.FromSeconds(1);
        public static readonly Time ExplostionDuration = Time.FromSeconds(.2);
        public static readonly Time TailDuration = Time.FromSeconds(2.5);
        public static readonly int TailsPerExplosion = 30;
        public static readonly float Gravity = 30;
        public static readonly Color[] FireworkColors = new Color[] { new Color(100, 255, 135), new Color(175, 255, 135), new Color(85, 190, 255), new Color(255, 145, 255), new Color(100, 100, 255), new Color(140, 250, 190), new Color(255, 135, 135), new Color(240, 255, 135), new Color(245, 215, 80) };
        private static readonly Time TimeStep = Time.FromSeconds(1d / 45d);
        #endregion

        #region Emitter
        private class FireworkEmitter : EmitterBase
        {
            #region Variables
            private static Random _rndgenerator = new Random();
            private Time _elapsedtime = Time.Zero;
            private Vector2f _position = new Vector2f(0, 0);
            private Color _color = Color.White;
            #endregion

            #region Constructors/Destructors
            public FireworkEmitter(Vector2f NewPosition)
            {
                _position = NewPosition;
                _color = FireworkColors[_rndgenerator.Next(0, FireworkColors.Length - 1)];
            }
            #endregion

            #region Functions
            public override void EmitParticles(ParticleSystem CurrentParticleSystem, Time ElapsedTime)
            {
                Time interval = ExplostionDuration / TailsPerExplosion;
                _elapsedtime += ElapsedTime;
                while (_elapsedtime - interval > Time.Zero)
                {
                    EmitTail(CurrentParticleSystem);
                    _elapsedtime -= interval;
                }
            }
            private void EmitTail(ParticleSystem CurrentParticleSystem)
            {
                PolarVector velocity = new PolarVector((float)_rndgenerator.Next(3000, 7000) / 100f, (float)_rndgenerator.Next(0, 360));
                float scale = .8f;
                for (int i = 0; i < 25; i++)
                {
                    Particle newparticle = new Particle(TailDuration);
                    newparticle.Position = _position;
                    newparticle.Color = _color;
                    newparticle.Scale = new Vector2f(scale, scale);
                    newparticle.Velocity = velocity;
                    base.EmitParticle(CurrentParticleSystem, newparticle);
                    velocity.R *= .96f;
                    scale *= .95f;
                }
            }
            #endregion
        }
        #endregion

        #region Affector
        private class FireworkAffector : IAffector
        {
            #region Functions
            public void ApplyAffector(Particle CurrentParticle, Time ElapsedTime)
            {
                CurrentParticle.Velocity = new Vector2f(CurrentParticle.Velocity.X, CurrentParticle.Velocity.Y + ((float)ElapsedTime.Seconds * Gravity * CurrentParticle.Scale.X * CurrentParticle.Scale.Y));
                CurrentParticle.Color = new Color(CurrentParticle.Color.R, CurrentParticle.Color.G, CurrentParticle.Color.B, (byte)(256f * CurrentParticle.RemainingRatio * CurrentParticle.Scale.X));
            }
            #endregion
        }
        #endregion

        #region Functions
        public void Run()
        {
            RenderWindow window = new RenderWindow(new VideoMode(800, 600), "NetEXT Fireworks Particle Example");
            window.SetVerticalSyncEnabled(true);
            Texture texture = new Texture(".\\particle.png");
            ParticleSystem system = new ParticleSystem(texture);
            system.AddAffector(new FireworkAffector());
            Random rnd = new Random();
            Timer explosiontimer = new Timer();
            explosiontimer.Restart(Time.FromSeconds(1));
            Clock frameclock = new Clock();
            Time acumtime = Time.Zero;
            while (window.IsOpen())
            {
                window.DispatchEvents();
                acumtime += frameclock.Restart();
                while (acumtime >= TimeStep)
                {
                    acumtime -= TimeStep;
                    system.Update(TimeStep);
                }
                if (explosiontimer.IsExpired)
                {
                    explosiontimer.Restart(ExplosionsInterval);
                    Vector2f pos = new Vector2f(rnd.Next(100, 700), rnd.Next(100, 500));
                    system.AddEmitter(new FireworkEmitter(pos), ExplostionDuration);
                }
                window.Clear(Color.Black);
                window.Draw(system, new RenderStates(BlendMode.Add));
                window.Display();
            }
        }
        #endregion
    }
}
