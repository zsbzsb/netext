﻿using NetEXT.Input;

namespace NetEXT.Examples
{
    public class User_Events_Example
    {
        #region Enums
        public enum CommandType
        {
            Move,
            Attack,
            HoldPosition
        }
        #endregion

        #region UnitEvent
        public class UnitEvent
        {
            public string UnitName;
            public CommandType Order;
            public UnitEvent (string UnitName, CommandType Order)
            {
                this.UnitName = UnitName;
                this.Order = Order;
            }
        }
        #endregion

        #region Functions
        public void Run()
        {
            EventSystem<CommandType, UnitEvent> system = new EventSystem<CommandType, UnitEvent>() { GetEventIDHandler = GetUnitEventID };
            system.Connect(CommandType.Move, OnMove);
            system.Connect(CommandType.Attack, OnAttack);
            system.Connect(CommandType.HoldPosition, OnHoldPosition);
            system.TriggerEvent(new UnitEvent("Tank", CommandType.Attack));
            system.TriggerEvent(new UnitEvent("Helicopter", CommandType.Move));
            system.TriggerEvent(new UnitEvent("Battleship", CommandType.Attack));
            system.TriggerEvent(new UnitEvent("Battleship", CommandType.HoldPosition));
            System.Console.ReadLine();
        }
        public CommandType GetUnitEventID(UnitEvent Event)
        {
            return Event.Order;
        }
        public void OnMove(UnitEvent Event)
        {
            System.Console.WriteLine("Unit " + Event.UnitName + " moves.");
        }
        public void OnAttack(UnitEvent Event)
        {
            System.Console.WriteLine("Unit " + Event.UnitName + " attacks.");
        }
        public void OnHoldPosition(UnitEvent Event)
        {
            System.Console.WriteLine("Unit " + Event.UnitName + " holds its position.");
        }
        #endregion
    }
}
